const Comment = require('./models').Comment;

module.exports = router => {

    router.get('/count', (req, res) => {
        console.log('hello count')
        Comment.count(req.query)
            .then(count => res.json(count))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message)
            })

    })

    // CRUD routes
    router.get('/', (req, res) => {
        const { sort, skip, limit, one, ...rest } = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }
        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        if (one) {
            q = Comment.findOne(rest);
        } else {
            q = Comment.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        q.then((comments) => {
            return res.json(comments);
        })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.post('/', (req, res) => {
        const data = req.body;
        Comment.create(data)
            .then((comment) => {
                return res.json(comment);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        Comment.update(conditions, { $set: values }, { ...options, multi: true })
            .then(() => Comment.find(conditions))
            .then(comments => {
                return res.json(comments);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/', (req, res) => {
        let conditions = req.body;
        let comments;
        Comment.find(conditions)
            .then((a) => {
                comments = a;
                return Comment.remove(conditions)
            })
            .then(() => {
                return res.json(comments);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.get('/:id', (req, res) => {
        Comment.findById(req.params.id)
            .then((comment) => {
                return res.json(comment);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        Comment.findByIdAndUpdate(id, { $set: changes })
            .then(() => Comment.findById(id))
            .then(comment => {
                return res.json(comment);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/:id', (req, res) => {
        const { id } = req.params;
        let deletedComment;
        Comment.findById(id)
            .then(comment => {
                deletedComment = comment;
                return Comment.findByIdAndRemove(id)
            })
            .then(() => {
                return res.json(deletedComment);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    return router;
}