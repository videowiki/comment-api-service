
const DB_CONNECTION = process.env.COMMENT_SERVICE_DATABASE_URL;
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;
const videowikiGenerators = require('@videowiki/generators');
const mongoose = require('mongoose');
const rabbitmqService = require('@videowiki/workers/vendors/rabbitmq');
const { server, app, createRouter } = require('./generateServer')();
const controller = require('./controller');

let mongoConnection;

mongoose.connect(DB_CONNECTION)
.then((con) => {
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })

    // initiate rabbitmq connection
    rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
        if (err) {
            throw err;
        }
        rabbitmqChannel = channel;
        channel.on('error', (err) => {
            console.log('RABBITMQ ERROR', err)
            process.exit(1);
        })
        channel.on('close', () => {
            console.log('RABBITMQ CLOSE')
            process.exit(1);
        })
        require('./rabbitmqHandlers').init(channel)
        // healthcheck route
        videowikiGenerators.healthcheckRouteGenerator({ router: app, mongoConnection, rabbitmqConnection: rabbitmqChannel.connection });

        app.use('/db', require('./dbRoutes')(createRouter()));

        app.all('*', (req, res, next) => {
            if (req.headers['vw-user-data']) {
                try {
                    const user = JSON.parse(req.headers['vw-user-data']);
                    req.user = user;
                } catch (e) {
                    console.log(e);
                }
            }
            next();
        })
        app.get('/by_article_id/:articleId', controller.getArticleComments);
        app.post('/', controller.addCommet)
    })
    

})
.catch(err => {
    console.log('error connecting to mongodb', err);
    process.exit(1);
})

const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
