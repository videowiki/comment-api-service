const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    article: { type: Schema.Types.ObjectId, index: true },
    user: { type: Schema.Types.ObjectId },
    isWhatsappComment: { type: Boolean, default: false },
    whatsappContactNumber: { type: String },
    
    content: { type: String },
    
    slidePosition: { type: Number },
    subslidePosition: { type: Number },
    created_at: { type: Number, default: Date.now },
})

// const CommentsThread = new Schema({
//     comments: [Comment]
// })

const Comment = mongoose.model('comment', CommentSchema);

module.exports = { Comment };